<?php
/*
Template Name: Category 
*/
get_header(); 



                $argsCats = array(
                    'post_type' => 'page',
                    'order' => 'ASC',
                    'orderby' => 'menu_order',
                    'meta_query' => array(
                        array(
                            'key' => 'useAsCat',
                            'value' => 1
                        )
                    )
                );

                $argsPP = array(
                    'post_type' => 'page',
                    'order' => 'ASC',
                    'orderby' => 'menu_order',
                    'meta_query' => array(
                        array(
                            'key' => 'useAsPP',
                            'value' => 1
                        )
                    )
                );

                $argsCW = array(
                    'post_type' => 'page',
                    'order' => 'ASC',
                    'orderby' => 'menu_order',
                    'meta_query' => array(
                        array(
                            'key' => 'useAsCW',
                            'value' => 1
                        )
                    )
                );

                $argsVid = array(
                    'post_type' => 'page',
                    'order' => 'ASC',
                    'orderby' => 'menu_order',
                    'meta_query' => array(
                        array(
                            'key' => 'useAsVid',
                            'value' => 1
                        )
                    )
                );

                $getCats = new WP_Query( $argsCats );
                $getPP = new WP_Query( $argsPP );
                $getCW = new WP_Query( $argsCW );
                $getVid = new WP_Query( $argsVid );

                ?>

                <div class="row projects">
                
                    <div class="small-3 large-3 columns cats">

                        <h2>Categories:</h2>
                        <?php wp_reset_postdata(); while ($getCats->have_posts()) : $getCats->the_post(); ?>
                            <?php echo the_title(); ?> 
                        <?php endwhile;?>

                    </div>

                    <div class="small-3 large-3 columns pp">

                        <h2>Personal Projects:</h2>
                        <?php wp_reset_postdata(); while ($getPP->have_posts()) : $getPP->the_post(); ?>
                            <?php echo get_the_title(); ?> 
                        <?php endwhile;?>

                    </div>

                    <div class="small-3 large-3 columns cw">

                        <h2>Commissioned Work:</h2>
                        <?php wp_reset_postdata(); while ($getCW->have_posts()) : $getCW->the_post(); ?>
                            <?php echo the_title(); ?> 
                        <?php endwhile; ?>


                    </div>

                    <div class="small-3 large-3 columns vid">
                    
                        <h2>Video:</h2>
                        <?php wp_reset_postdata(); while ($getVid->have_posts()) : $getVid->the_post(); ?>
                            <?php echo the_title(); ?> 
                        <?php endwhile;
                        wp_reset_postdata(); ?>

                    </div>

                </div>

   <?php get_footer(); 
    ?>