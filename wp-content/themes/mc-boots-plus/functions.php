<?php

/*

Author: Zhen Huang

URL: http://themefortress.com/



This place is much cleaner. Put your theme specific codes here,

anything else you may wan to use plugins to keep things tidy.



*/



require_once('lib/clean.php'); // do all the cleaning and enqueue here



require_once('lib/enqueue-sass.php'); // do all the cleaning and enqueue if you Sass to customize Reverie



require_once('lib/foundation.php'); // load Foundation specific functions like top-bar



require_once('lib/presstrends.php'); // load PressTrends to track the usage of Reverie across the web, comment this line if you don't want to be tracked





/**********************

Add theme supports

**********************/

function reverie_theme_support() {

	// Add language supports.

	load_theme_textdomain('reverie', get_template_directory() . '/lang');

	

	// Add post thumbnail supports. http://codex.wordpress.org/Post_Thumbnails

	add_theme_support('post-thumbnails');

	// set_post_thumbnail_size(150, 150, false);

	

	// rss thingy

	add_theme_support('automatic-feed-links');

	

	// Add post formarts supports. http://codex.wordpress.org/Post_Formats

	add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

	

	// Add menu supports. http://codex.wordpress.org/Function_Reference/register_nav_menus

	add_theme_support('menus');

	register_nav_menus(array(

		'primary' => __('Primary Navigation', 'reverie'),

		'utility' => __('Utility Navigation', 'reverie')

	));

	

	// Add custom background support

	add_theme_support( 'custom-background',

	    array(

	    'default-image' => '',  // background image default

	    'default-color' => '', // background color default (dont add the #)

	    'wp-head-callback' => '_custom_background_cb',

	    'admin-head-callback' => '',

	    'admin-preview-callback' => ''

	    )

	);

}

add_action('after_setup_theme', 'reverie_theme_support'); /* end Reverie theme support */



// create widget areas: sidebar, footer

$sidebars = array('Sidebar');

foreach ($sidebars as $sidebar) {

	register_sidebar(array('name'=> $sidebar,

		'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="small-12 columns">',

		'after_widget' => '</div></article>',

		'before_title' => '<h6><strong>',

		'after_title' => '</strong></h6>'

	));

}

$sidebars = array('Footer');

foreach ($sidebars as $sidebar) {

	register_sidebar(array('name'=> $sidebar,

		'before_widget' => '<article id="%1$s" class="large-4 columns widget %2$s">',

		'after_widget' => '</article>',

		'before_title' => '<h6><strong>',

		'after_title' => '</strong></h6>'

	));

}



// return entry meta information for posts, used by multiple loops.

function reverie_entry_meta() {

	echo '<time class="updated" datetime="'. get_the_time('c') .'" pubdate>'. sprintf(__('Posted on %s at %s.', 'reverie'), get_the_time('l, F jS, Y'), get_the_time()) .'</time>';

	echo '<p class="byline author">'. __('Written by', 'reverie') .' <a href="'. get_author_posts_url(get_the_author_meta('ID')) .'" rel="author" class="fn">'. get_the_author() .'</a></p>';

}







/* -- JHP Carousel -- */



require_once('lib/jhp-caro.php');



/* -- JHP Curtain -- */



require_once('lib/jhp-curtain.php');



add_action( 'add_meta_boxes', 'homepage_meta' );

function homepage_meta() {

    add_meta_box(

        'homepage-meta',          // this is HTML id of the box on edit screen

        'Homepage Section Details',    // title of the box

        'homepage_meta_box',   // function to be called to display the checkboxes, see the function below

        'page',        // on which edit screen the box should appear

        'side',      // part of page where the box should appear

        'default'      // priority of the box

    );

}



function homepage_meta_box( ) {



    wp_nonce_field( plugin_basename( __FILE__ ), 'homepage_meta_nonce' );

	global $post;

	$homeCurtain_value = get_post_meta($post->ID, 'homeCurtain', true);
	if($homeCurtain_value == '1') { $homeCurtain_checked = 'checked="checked"'; } 

    echo '<input type="checkbox" name="homeCurtain" value="1" ' . $homeCurtain_checked . ' /> Use as <strong>Homepage Curtain</strong>?<br />';



	$useAsCat_value = get_post_meta($post->ID, 'useAsCat', true);
	if($useAsCat_value == '1') { $useAsCat_checked = 'checked="checked"'; } 

    echo '<input type="checkbox" name="useAsCat" value="1" ' . $useAsCat_checked . ' /> Use as <strong>Category</strong> Page?<br />';


	$useAsPP_value = get_post_meta($post->ID, 'useAsPP', true);
	if($useAsPP_value == '1') { $useAsPP_checked = 'checked="checked"'; } 

    echo '<input type="checkbox" name="useAsPP" value="1" ' . $useAsPP_checked . ' /> Use as <strong>Personal Project</strong> Page?<br />';


	$useAsCW_value = get_post_meta($post->ID, 'useAsCW', true);
	if($useAsCW_value == '1') { $useAsCW_checked = 'checked="checked"'; } 

    echo '<input type="checkbox" name="useAsCW" value="1" ' . $useAsCW_checked . ' /> Use as <strong>Commissioned Work</strong> Page?<br />';


	$useAsVid_value = get_post_meta($post->ID, 'useAsVid', true);
	if($useAsVid_value == '1') { $useAsVid_checked = 'checked="checked"'; } 

    echo '<input type="checkbox" name="useAsVid" value="1" ' . $useAsVid_checked . ' /> Use as <strong>Video</strong> Page?<br />';


}



// save data from checkboxes

add_action( 'save_post', 'homepage_meta_data' );

function homepage_meta_data( $post_id ) {



    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){

        return;

    }



    // security check

    if ( !wp_verify_nonce( $_POST['homepage_meta_nonce'], plugin_basename( __FILE__ ) ) ){

        return;

    }



    // further checks if you like, 

    // for example particular user, role or maybe post type in case of custom post types



    // now store data in custom fields based on checkboxes selected

    if ( isset( $_POST['homeCurtain'] ) ) {
        update_post_meta( $post_id, 'homeCurtain', 1 );
    } else {
        update_post_meta( $post_id, 'homeCurtain', 0 );
    }

    if ( isset( $_POST['useAsCat'] ) ) {
        update_post_meta( $post_id, 'useAsCat', 1 );
    } else {
        update_post_meta( $post_id, 'useAsCat', 0 );
    }

    if ( isset( $_POST['useAsPP'] ) ) {
        update_post_meta( $post_id, 'useAsPP', 1 );
    } else {
        update_post_meta( $post_id, 'useAsPP', 0 );
    }

    if ( isset( $_POST['useAsCW'] ) ) {
        update_post_meta( $post_id, 'useAsCW', 1 );
    } else {
        update_post_meta( $post_id, 'useAsCW', 0 );
    }

    if ( isset( $_POST['useAsVid'] ) ) {
        update_post_meta( $post_id, 'useAsVid', 1 );
    } else {
        update_post_meta( $post_id, 'useAsVid', 0 );
    }



}



?>