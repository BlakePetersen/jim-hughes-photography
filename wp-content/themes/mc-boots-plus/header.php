<!doctype html>

<head>

	<meta charset="utf-8">

	<title><?php wp_title('|', true, 'right'); bloginfo('name'); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=0.8, minimum-scale=0.8, maximum-scale=0.8" />

	<!-- Favicon and Feed -->

	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">



	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/foundation.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mc.css">

	<?php wp_head(); ?>


	<script data-main="<?php echo get_template_directory_uri();?>/js/common" src="<?php echo get_template_directory_uri();?>/js/lib/require/require.js"></script>


	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-44952047-1', 'jimhughesphoto.com');
	  ga('send', 'pageview');

	</script>

</head>

<body <?php body_class(); ?>>

	<?php if (is_front_page() == 1 && !isset($_COOKIE['firstViewedEver'])) { ?>
	
		<div class="loading logo" >

    		<div class="inner" id="firstViewLoading">
        		<svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0" y="0" width="755" height="150" viewBox="0 0 742 143" enable-background="new 0 0 742 143" xml:space="preserve" class="svg-logo" alt="Jim Hughes Photography"><path d="M69.9 76.4c0 18.4-4.5 36.7-34.9 36.7 -22.4 0-31.6-14.7-31.6-34v-5.6h18.6v5.5c0 12 3.3 19 14.6 19 12 0 14.9-7.3 14.9-20.5V4.5h18.6V76.4zM82.1 4.5H99v16H82.1V4.5zM82.1 33.8H99v76.8H82.1V33.8z"/><path d="M110.8 33.8h16v10.7h0.4c5.1-7.6 11.4-12.8 23.6-12.8 9.4 0 18.1 4 21.5 12.8 5.6-7.9 12.9-12.8 24.1-12.8 16.2 0 25.7 7.1 25.7 25.9v53h-16.9V65.7c0-12.2-0.7-20.7-14-20.7 -11.4 0-16.3 7.6-16.3 20.5v45h-16.9V61.3c0-10.5-3.3-16.2-13.5-16.2 -8.8 0-16.8 7.1-16.8 19.9v45.6h-16.9V33.8z"/><path d="M261 4.5h18.6v42.2h48.3V4.5h18.6v106.1h-18.6V62.7h-48.3v47.8h-18.6V4.5zM424.7 110.6h-16.6V99.9h-0.3c-4.2 7.7-13.4 12.8-21.8 12.8 -20.1 0-28.7-10.1-28.7-30.2V33.8h16.9v47.1c0 13.5 5.5 18.4 14.7 18.4 14.1 0 18.9-9.1 18.9-20.9V33.8h16.9V110.6z"/><path d="M507.7 106.6c0 23-13.5 34.5-37.7 34.5 -15.4 0-32.8-5.9-34.3-23.9h16.9c2.1 9.7 9.8 11.3 18.4 11.3 13.7 0 19.8-7 19.8-19.6V97.1h-0.3c-4.8 8.5-13.5 13.5-23 13.5 -24.4 0-34.5-18.4-34.5-40.4 0-20.7 12.9-38.5 34.8-38.5 9.7 0 18.4 4 22.7 12.6h0.3V33.8h16.9V106.6zM490.8 70.5c0-13.2-5.9-25.4-20.6-25.4 -15 0-20.2 14-20.2 26.7 0 12.6 6.1 25.4 20.2 25.4C485.2 97.2 490.8 83.5 490.8 70.5zM517.5 4.5h16.9V43.9h0.3c4.2-7 12.9-12.2 23-12.2 16.6 0 27.2 8.9 27.2 26.1v52.7h-16.9V62.3c-0.3-12-5-17.2-15-17.2 -11.3 0-18.6 8.9-18.6 20.2v45.3h-16.9V4.5z"/><path d="M610.3 76.6c0 12 6.5 22.7 20.7 22.7 9.8 0 15.7-4.3 18.7-12.8h16c-3.7 16.8-18 26.1-34.8 26.1 -24.1 0-37.6-16.8-37.6-40.4 0-21.8 14.3-40.6 37.1-40.6 24.2 0 39.1 21.8 36.2 44.9H610.3zM649.8 65.4c-0.6-10.7-7.9-20.4-19.3-20.4 -11.7 0-19.8 8.9-20.2 20.4H649.8zM689.6 85.9c0.9 9.8 8.3 13.4 17.4 13.4 6.4 0 17.5-1.3 17.1-10.1 -0.4-8.9-12.8-10-25.1-12.8 -12.5-2.7-24.7-7.1-24.7-22.7 0-16.8 18.1-22 32.1-22 15.8 0 30 6.5 32.1 23.6h-17.7c-1.5-8-8.2-10.3-15.6-10.3 -4.9 0-14 1.2-14 7.9 0 8.3 12.5 9.5 25 12.3 12.3 2.8 24.8 7.3 24.8 22.4 0 18.3-18.4 25-34.3 25 -19.3 0-33.7-8.6-34-26.7H689.6z"/></svg>
	            <h2>Photographer + Director</h2>
	        </div>

    	</div>

    <?php } else { ?>

        <div class="loading caro" id="loading-caro">

            <div class="loader" id="spinner">Loading...</div>

        </div>

    <?php } ?>

<div class="contain-to-grid" id="top-container">

	<div class="off-canvas-wrap">

		<div class="inner-wrap">

			<nav class="tab-bar">

				<section class="left-small">

				    <span class="left-off-canvas-toggle-edit" >&#xE690;</span>

				    	<h2 class="entry-title"><?php the_title(); ?></h2>
				     
				</section>

				<section class="tab-bar-section">

				  	<h1>

				  		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"  title="Jim Hughes Photography">

				  			<img src="<?php echo get_template_directory_uri() . '/img/JHP-logo.svg' ?>" class="logo" alt="Jim Hughes Photography" />

				  		</a>

				  	</h1>

				</section>

			</nav>

			<aside class="left-off-canvas-menu">

		    <?php

		        wp_nav_menu( array(

		            'theme_location' => 'primary',

		            'container' => false,

		            'depth' => 0,

		            'items_wrap' => '<ul class="off-canvas-list">%3$s</ul>',

		            'walker' => new reverie_walker( array(

		                'in_top_bar' => true,

		                'item_type' => 'li'

		            ) ),

		        ) );

		    ?>

			</aside>


<section class="container" role="document">

	<div class="row">