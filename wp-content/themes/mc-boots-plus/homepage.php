<?php
/*
Template Name: Homepage 
*/
get_header(); 

$args = array(
    'post_type' => 'page',
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'meta_query' => array(
        array(
            'key' => 'homeCurtain',
            'value' => 1
        )
    )
);
$getCurtains = new WP_Query( $args );

?>

	<div class="small-12 large-12 columns" role="main">

	<?php while ($getCurtains->have_posts()) : $getCurtains->the_post(); ?>

		<div class="curtain down">
            <?php if ($post->ID != 332) {

                    the_content();

                } else {

                $argsCats = array(
                    'post_type' => 'page',
                    'posts_per_page' => -1,
                    'order' => 'ASC',
                    'orderby' => 'title',
                    'meta_query' => array(
                        array(
                            'key' => 'useAsCat',
                            'value' => 1
                        )
                    )
                );

                $argsPP = array(
                    'post_type' => 'page',
                    'posts_per_page' => -1,
                    'order' => 'ASC',
                    'orderby' => 'title',
                    'meta_query' => array(
                        array(
                            'key' => 'useAsPP',
                            'value' => 1
                        )
                    )
                );

                $argsCW = array(
                    'post_type' => 'page',
                    'posts_per_page' => -1,
                    'order' => 'ASC',
                    'orderby' => 'title',
                    'meta_query' => array(
                        array(
                            'key' => 'useAsCW',
                            'value' => 1
                        )
                    )
                );


                $getCats = new WP_Query( $argsCats );
                $getPP = new WP_Query( $argsPP );
                $getCW = new WP_Query( $argsCW );

                ?>

                <div class="row projects">
                
                    <div class="hide-for-small-only large-4 columns cats">

                        <h3>Categories:</h3>
                        <div>
                            <a href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/portfolio/">
                                Portfolio                                
                            </a>
                        </div>
                        <?php wp_reset_postdata(); while ($getCats->have_posts()) : $getCats->the_post(); ?>
                            <div>
                                <a href="<?php the_permalink(); ?>">
                                    <?php 
                                        
                                        the_title(); 

                                        $isVid = get_post_meta($post->ID, 'useAsVid', TRUE);

                                        if($isVid != 0) { ?>
                                            
                                            <span><img src="<?php echo get_template_directory_uri(); ?>/img/video-icon.gif" /></span>

                                    <?php } ?>
                                </a>
                            </div>
                        <?php endwhile;?>

                    </div>

                    <div class="hide-for-small-only large-4 columns pp">

                        <h3>Personal Projects:</h3>
                        <?php wp_reset_postdata(); while ($getPP->have_posts()) : $getPP->the_post(); ?>
                            <div>
                                <a href="<?php the_permalink(); ?>">
                                    <?php 
                                        
                                        the_title(); 

                                        $isVid = get_post_meta($post->ID, 'useAsVid', TRUE);

                                        if($isVid != 0) { ?>
                                            
                                            <span><img src="<?php echo get_template_directory_uri(); ?>/img/video-icon.gif" /></span>

                                    <?php } ?>
                                </a>
                            </div>
                        <?php endwhile;?>

                    </div>

                    <div class="hide-for-small-only large-4 columns cw">

                        <h3>Commissioned Work:</h3>
                        <?php wp_reset_postdata(); while ($getCW->have_posts()) : $getCW->the_post(); ?>
                            <div>
                                <a href="<?php the_permalink(); ?>">
                                    <?php 
                                        
                                        the_title(); 

                                        $isVid = get_post_meta($post->ID, 'useAsVid', TRUE);

                                        if($isVid != 0) { ?>
                                            
                                            <span><img src="<?php echo get_template_directory_uri(); ?>/img/video-icon.gif" /></span>

                                    <?php } ?>
                                </a>
                            </div>
                        <?php endwhile; ?>


                    </div>

                    <div class="show-for-small-only row text-center">

                        <div class="columns small-12">
                            <a class="btn" data-reveal-id="catsModal">Categories</a>
                        </div>
                        <div class="columns small-12">
                            <a class="btn" data-reveal-id="ppModal">Personal Projects</a>
                        </div>
                        <div class="columns small-12">
                            <a class="btn" data-reveal-id="cwModal">Commissioned Work</a>
                        </div>

                    </div>

                    <div id="catsModal" class="reveal-modal" data-reveal aria-labelledby="catsModalTitle" aria-hidden="true" role="dialog">

                        <h2 id="catsModalTitle">Categories</h2>
                        <div>

                        <?php wp_reset_postdata(); while ($getCats->have_posts()) : $getCats->the_post(); ?>
                                <a href="<?php the_permalink(); ?>">
                                    <?php

                                    the_title();

                                    $isVid = get_post_meta($post->ID, 'useAsVid', TRUE);

                                    if($isVid != 0) { ?>

                                        <span><img src="<?php echo get_template_directory_uri(); ?>/img/video-icon.gif" /></span>

                                    <?php } ?>
                                </a>
                        <?php endwhile;?>
                        </div>
                        <a class="close-reveal-modal" aria-label="Close">&#215;</a>

                    </div>

                    <div id="ppModal" class="reveal-modal" data-reveal aria-labelledby="ppModalTitle" aria-hidden="true" role="dialog">

                        <h2 id="ppModalTitle">Personal Projects</h2>
                        <div>

                        <?php wp_reset_postdata(); while ($getPP->have_posts()) : $getPP->the_post(); ?>
                                <a href="<?php the_permalink(); ?>">
                                    <?php

                                    the_title();

                                    $isVid = get_post_meta($post->ID, 'useAsVid', TRUE);

                                    if($isVid != 0) { ?>

                                        <span><img src="<?php echo get_template_directory_uri(); ?>/img/video-icon.gif" /></span>

                                    <?php } ?>
                                </a>
                        <?php endwhile;?>
                        </div>
                        <a class="close-reveal-modal" aria-label="Close">&#215;</a>

                    </div>

                    <div id="cwModal" class="reveal-modal" data-reveal aria-labelledby="cwModalTitle" aria-hidden="true" role="dialog">

                        <h2 id="cwModalTitle">Commissioned Work</h2>
                        <div>

                        <?php wp_reset_postdata(); while ($getCW->have_posts()) : $getCW->the_post(); ?>
                                <a href="<?php the_permalink(); ?>">
                                    <?php

                                    the_title();

                                    $isVid = get_post_meta($post->ID, 'useAsVid', TRUE);

                                    if($isVid != 0) { ?>

                                        <span><img src="<?php echo get_template_directory_uri(); ?>/img/video-icon.gif" /></span>

                                    <?php } ?>
                                </a>
                        <?php endwhile; ?>

                    </div>                        <a class="close-reveal-modal" aria-label="Close">&#215;</a>

                    </div>


                </div>

            <?php } ?>

        </div>

    <?php 

        endwhile;
        get_footer(); 

    ?>