var $ = jQuery;

$('.jhp-caro').addClass("caro-1");

var windowHeight = window.innerHeight;
var windowWidth = window.innerWidth;

$('li, .container').on('dragstart', function(event) {
    event.preventDefault();
});

$('.open-thumb-bank, .close-thumb-bank').on('click',function() {
    toggleThumbBank(400);
});

function toggleThumbBank(speed) {
    $('.thumb-bank, .menu-main-container').toggleClass('open');
    $('.open-thumb-bank, .navi, .top-bar, section.left-small, .tab-bar-section').fadeToggle(speed);
}

function getBackgroundImageDimensions (imageSrc) {

    imageSrc = $(imageSrc)
        .css('background-image')
        .replace(/url\((['"])?(.*?)\1\)/gi, '$2');

    var image = new Image();
    image.src = imageSrc;

    var width = image.width,
        height = image.height,
        aspect = image.width / image.height;

    delete(image);

    return {
        width: width,
        height: height,
        aspect: aspect
    };

}

function revealCaro() {
    $('.loading.caro').addClass('hide');
}

function Carousel(element) {

    var self = this;
    element = $(element);

    var panes = $(">ul>li", element),
        caroWidth = windowWidth,
        paneWidth = windowWidth,
        paneCount = panes.length;

    this.getCurrentActivePane = function() {
        var currentPane = $('.caro-1 .pane[role=active]').attr('data-carousel');
        return Number(currentPane);
    };

    this.stripActivePane = function() {
        $('.pane[role=active]').removeAttr('role', 'active');
    };

    this.grantActivePane = function(i) {
        $('.pane[role=active]').removeAttr('role', 'active');
        $('.pane-' + i).attr('role', 'active');
    };

    element.hammer({
        drag_lock_to_axis: true
    }).on("release dragleft dragright swipeleft swiperight", handleHammer);


    function handleHammer(ev) {

        ev.gesture.preventDefault();
        var current = self.getCurrentActivePane();

        switch (ev.type) {
            case 'dragright':

                p = current - 1;
                $('.caro-1 .pane-' + p).show();
                caroDragOffset = (100 / caroWidth) * ev.gesture.deltaX;
                if ((current == 1 && ev.gesture.direction == Hammer.DIRECTION_RIGHT)) {
                    caroDragOffset *= 0;
                }
                self.setContainerOffset(current - 1, -100 + caroDragOffset, false);

                break;

            case 'dragleft':

                caroDragOffset = (100 / caroWidth) * ev.gesture.deltaX;
                if ((current == paneCount && ev.gesture.direction == Hammer.DIRECTION_LEFT)) {
                    caroDragOffset *= 0;
                }
                self.setContainerOffset(current, caroDragOffset, false);
                break;

            case 'swipeleft':
                self.next(current);
                ev.gesture.stopDetect();
                break;

            case 'swiperight':
                self.prev(current);
                ev.gesture.stopDetect();
                break;

            case 'release':
                if (Math.abs(ev.gesture.deltaX) > paneWidth / 3) {

                    if (ev.gesture.direction == 'right') {

                        self.prev(current);

                    } else {

                        self.next(current);

                    }

                } else {

                    self.rollInPane(current);

                }

                ev.gesture.stopDetect();

                break;
        }
    }

    this.paneOn = function(c, d, jumpTo){

        setTimeout(function(){

            $('.pane-' + c).removeClass('drag off').addClass('on').css('margin-left','');
            $('.pane-' + (c+3)).removeClass('off on').css('margin-left','');

        }, ((500 * d) / Math.abs(jumpTo)));

    };

    this.paneOff = function(j, k, jumpTo){

        setTimeout(function(){

            $('.pane-' + j).removeClass('drag on').addClass('off').css('margin-left','');
            $('.pane-' + (j+3)).removeClass('off').addClass('on');

        }, ((500 * k) / Math.abs(jumpTo)));

    };

    this.showPane = function(i) {

        var currentPane = self.getCurrentActivePane();
        var jumpTo = i - currentPane;

        self.stripActivePane();

        if (jumpTo < 0) {

            if ((jumpTo - 1) == -paneCount) {
                $('.off-canvas-wrap').addClass('move-right');
            }

            for (var j = (currentPane - 1); j >= i; j--) {

                var k = Math.abs(jumpTo) - (j - i) - 1;

                self.paneOn(j, k, jumpTo);

            }

        }

        if (jumpTo > 0) {

            for (var l = currentPane; l < i; l++) {

                var m = jumpTo + (l - i);

                self.paneOff(l, m, jumpTo);

            }

        }

        self.grantActivePane(i);

    };


    this.rollInPane = function(i) {
        $('.pane-' + i).removeClass('off drag').addClass('on').attr('role', 'active');
    };


    this.setContainerOffset = function(i, percent) {
        var px = ((caroWidth) / 100) * percent;
        $('.pane-' + i).removeClass('on off down').addClass('drag').css('margin-left', px + 'px');
    };

    this.next = function(i) {
        if (i == paneCount) {
            return this.showPane(1);
        } else {
            return this.showPane(i + 1);
        }
    };

    this.prev = function(i) {
        if (i == 1) {
            return this.showPane(paneCount);
        } else {
            return this.showPane(i - 1);
        }
    };


    $('.navi.back').on('click', function() {
        var current = self.getCurrentActivePane();
        return self.prev(current);
    });
    $('.navi.forw').on('click', function() {
        var current = self.getCurrentActivePane();
        return self.next(current);
    });

    $('.thumb-bank li').on('click', function() {
        var id = $(this).attr('class');
        id = id.replace('jhp-caro-', '');
        id = Number(id) + 1;
        self.showPane(id, true);
        return toggleThumbBank('slow');
    });


    function setUpThumbs() {

        if (imgCount == 1) {
            $('.open-thumb-bank, .navi').addClass('hide');
        }

        var paneH = windowHeight,
            paneW = windowWidth,
            numRows = 0,
            borderWidth = 3;

        if (paneH < paneW) {

            if (imgCount < 4) {
                numRows = 1;
            } else if (imgCount >= 4 && imgCount < 10) {
                numRows = 2;
            } else if (imgCount >= 10 && imgCount < 15) {
                numRows = 3;
            } else if (imgCount >= 15 && imgCount < 22) {
                numRows = 4;
            } else if (imgCount >= 22 && imgCount < 29) {
                numRows = 5;
            } else if (imgCount >= 29 && imgCount < 36) {
                numRows = 5;
            } else if (imgCount >= 36 && imgCount < 43) {
                numRows = 6;
            } else if (imgCount >= 43 && imgCount < 50) {
                numRows = 6;
            } else if (imgCount >= 50 && imgCount < 57) {
                numRows = 7;
            } else if (imgCount >= 57 && imgCount < 64) {
                numRows = 7;
            } else {
                numRows = 8;
            }

        } else {

            if (imgCount < 4) {
                numRows = 1;
            } else if (imgCount >= 4 && imgCount < 8) {
                numRows = 2;
            } else if (imgCount >= 8 && imgCount < 12) {
                numRows = 3;
            } else if (imgCount >= 12 && imgCount < 16) {
                numRows = 4;
            } else if (imgCount >= 16 && imgCount < 20) {
                numRows = 5;
            } else if (imgCount >= 20 && imgCount < 24) {
                numRows = 6;
            } else if (imgCount >= 24 && imgCount < 28) {
                numRows = 7;
            } else if (imgCount >= 28 && imgCount < 32) {
                numRows = 10;
            } else if (imgCount >= 32 && imgCount < 36) {
                numRows = 10;
            } else if (imgCount >= 36 && imgCount < 40) {
                numRows = 11;
            } else if (imgCount >= 40 && imgCount < 44) {
                numRows = 11;
            } else {
                numRows = 12;
            }
        }


        var r = Math.ceil(paneCount / numRows);
        var r1 = '.jhp-caro-' + r,
            r2 = '.jhp-caro-' + (2 * r),
            r3 = '.jhp-caro-' + (3 * r),
            r4 = '.jhp-caro-' + (4 * r),
            r5 = '.jhp-caro-' + (5 * r),
            r6 = '.jhp-caro-' + (6 * r),
            r7 = '.jhp-caro-' + (7 * r),
            r8 = '.jhp-caro-' + (8 * r),
            r9 = '.jhp-caro-' + (9 * r),
            r10 = '.jhp-caro-' + (10 * r),
            r11 = '.jhp-caro-' + (10 * r),
            r12 = '.jhp-caro-' + (10 * r);


        $(r1).css('clear', 'left');
        $(r2).css('clear', 'left');
        $(r3).css('clear', 'left');
        $(r4).css('clear', 'left');
        $(r5).css('clear', 'left');
        $(r6).css('clear', 'left');
        $(r7).css('clear', 'left');
        $(r8).css('clear', 'left');
        $(r9).css('clear', 'left');
        $(r10).css('clear', 'left');
        $(r11).css('clear', 'left');
        $(r12).css('clear', 'left');


        var i1 = 0, i2 = 0, i3 = 0, i4 = 0, i5 = 0, i6 = 0, i7 = 0, i8 = 0, i9 = 0, i10 = 0, i11 = 0, i12 = 0, w1 = 0, w2 = 0, w3 = 0, w4 = 0, w5 = 0, w6 = 0, w7 = 0, w8 = 0, w9 = 0, w10 = 0, w11 = 0, w12 = 0;

        // global image heights based on row numbers //
        var ih = Math.floor((paneH - ((numRows - 1) * (borderWidth * 2))) / numRows);

        var i = 0;

        $('.thumb-bank li').each(function() {

            $(this).height(ih);

            var imgWidth = getBackgroundImageDimensions(this).width;

            if (i < r) {
                i1++;
                w1 += imgWidth;
            } else if (i < (2 * r)) {
                i2++;
                w2 += imgWidth;
            } else if (i < (3 * r)) {
                i3++;
                w3 += imgWidth;
            } else if (i < (4 * r)) {
                i4++;
                w4 += imgWidth;
            } else if (i < (5 * r)) {
                i5++;
                w5 += imgWidth;
            } else if (i < (6 * r)) {
                i6++;
                w6 += imgWidth;
            } else if (i < (7 * r)) {
                i7++;
                w7 += imgWidth;
            } else if (i < (8 * r)) {
                i8++;
                w8 += imgWidth;
            } else if (i < (9 * r)) {
                i9++;
                w9 += imgWidth;
            } else if (i < (10 * r)) {
                i10++;
                w10 += imgWidth;
            } else if (i < (11 * r)) {
                i11++;
                w11 += imgWidth;
            } else if (i < (12 * r)) {
                i12++;
                w12 += imgWidth;
            }

            i++;

        });


        // find percentage to reduce image size based on total row width proportion to $(window).width()
        var reduceByRow1 = ((w1 - (paneW - ((i1) * (borderWidth * 2)))) / w1);
        var reduceByRow2 = ((w2 - (paneW - ((i2) * (borderWidth * 2)))) / w2);
        var reduceByRow3 = ((w3 - (paneW - ((i3) * (borderWidth * 2)))) / w3);
        var reduceByRow4 = ((w4 - (paneW - ((i4) * (borderWidth * 2)))) / w4);
        var reduceByRow5 = ((w5 - (paneW - ((i5) * (borderWidth * 2)))) / w5);
        var reduceByRow6 = ((w6 - (paneW - ((i6) * (borderWidth * 2)))) / w6);
        var reduceByRow7 = ((w7 - (paneW - ((i7) * (borderWidth * 2)))) / w7);
        var reduceByRow8 = ((w8 - (paneW - ((i8) * (borderWidth * 2)))) / w8);
        var reduceByRow9 = ((w9 - (paneW - ((i9) * (borderWidth * 2)))) / w9);
        var reduceByRow10 = ((w10 - (paneW - ((i10) * (borderWidth * 2)))) / w10);
        var reduceByRow11 = ((w11 - (paneW - ((i11) * (borderWidth * 2)))) / w11);
        var reduceByRow12 = ((w12 - (paneW - ((i12) * (borderWidth * 2)))) / w12);


        i = 0;
        var ratio = 0, cont_width, backgroundDimensions, cont_init_width;

        $('.thumb-bank li').each(function() {

            backgroundDimensions = getBackgroundImageDimensions(this);

            cont_init_width = backgroundDimensions.width;

            if (i < r) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow1));

            } else if (i < (2 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow2));

            } else if (i < (3 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow3));

            } else if (i < (4 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow4));

            } else if (i < (5 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow5));

            } else if (i < (6 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow6));

            } else if (i < (7 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow7));

            } else if (i < (8 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow8));

            } else if (i < (9 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow9));

            } else if (i < (10 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow10));

            } else if (i < (11 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow11));

            } else if (i < (12 * r)) {

                cont_width = (backgroundDimensions.width - (backgroundDimensions.width * reduceByRow12));

            } else {

                cont_width = backgroundDimensions.width;
            }

            cont_width = Math.ceil(cont_width);
            ratio = (cont_width + 3) / backgroundDimensions.width;

            $(this).css("width", cont_width + 3);

            i++;

        });

    }


    $(window).on('load', function(){

        revealCaro();

        $('.pane').each(function(){

            var aspect = getBackgroundImageDimensions(this).aspect;
            if (aspect < 1.2) {
                $(this).css('background-size','contain');
            }

        });

        setUpThumbs();

    });

}

new Carousel('.jhp-caro');