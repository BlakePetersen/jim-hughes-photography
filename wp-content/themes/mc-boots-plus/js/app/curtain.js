

/* --##-- Curtain Layout Setup --##-- */
var page = $(document);
var viewport = {
    height: window.innerHeight,
    width: window.innerWidth
};
var curtains = $(".curtain", page);
var curtain = 1;
var curtainCount = curtains.length;

$('.container .columns[role=main], .container .columns[role=main] ul.main, .justified-image-grid').css('height', viewport.height);
$('.left-off-canvas-menu').css('height', $('ul.off-canvas-list').height());


$('.curtain').each(function() {

    $(this).addClass("curtain-" + curtain);
    $(this).attr("data-curtain", curtain);

    if (curtain == 1) {
        $(this).attr('role', 'active');
    }

    curtainCount--;
    curtain++;

});


/* --##-- Curtain Core Method --##-- */
var Curtain = function(element) {

    var self = this;
    element = $(element);

    var curtCount = element.length;
    var curtHeight = viewport.height;

    updateHash = function(i) {
        var hash = $('.menu-curtain:eq(' + (i - 1) + ') a').attr('href').split('#').pop();
        document.location.hash = hash;
        var hashtag = location.hash;

        $('.menu-item.active').removeClass('act');
        $('.menu-item a[href$="' + hashtag + '"]').parent('.menu-item').addClass('act');

        var linkText = $('.menu-item a[href$="' + hashtag + '"]').text();
        $('.tab-bar h2.entry-title').text(linkText);
    };

    getCurrentActiveCurt = function() {
        var currentCurt = $('.curtain[role=active]').attr('data-curtain');
        return Number(currentCurt);
    };

    getActivePositionCurt = function(p) {
        var pos = $('.curtain-' + p);
        var position = pos.css('margin-top');
        position = parseFloat(position);
        return position;
    };

    stripActiveCurt = function() {
        $('.curtain[role=active]').removeAttr('role');
    };

    grantActiveCurt = function(i) {
        stripActiveCurt();
        $('.curtain-' + i).attr('role', 'active');
    };

    getCurtainNumber = function(i) {
        var curtainNumber = $(i).attr('data-curtain');
        return Number(curtainNumber);
    };

    curtUp = function(c, d, jumpTo){

        setTimeout(function(){

            $('.curtain-' + c).removeClass('drag down scroll').addClass('up');

        }, ((500 * d) / Math.abs(jumpTo)));

        return false;

    };

    curtDown = function(j, k, jumpTo){
        
        setTimeout(function(){

            $('.curtain-' + j).removeClass('drag up scroll').addClass('down');
        
        }, ((500 * k) / Math.abs(jumpTo)));

        return false;

    };

    showCurt = function(i) {

        var currentCurt = getCurrentActiveCurt();
        var jumpTo = i - currentCurt;
        grantActiveCurt(i);

        if (jumpTo < 0) {

            for (var j = (currentCurt - 1); j >= i; j--) {

                var k = Math.abs(jumpTo) - (j - i) - 1;

                curtDown(j, k, jumpTo);

            }

        }

        if (jumpTo > 0) {

            for (var c = currentCurt; c < i; c++) {

                var d = jumpTo + (c - i);

                curtUp(c, d, jumpTo);

            }

        }

        return false;

    };

    rollUpCurtain = function(i) {
        $('.curtain-' + i).removeClass('down drag scroll').addClass('up');
        return false;
    };

    rollDownCurtain = function(i) {
        $('.curtain-' + i).removeClass('up drag scroll').addClass('down');
        return false;
    };

    setCurtainOffset = function(i, percent) {
        var px = ((curtHeight) / 100) * percent;
        $('.curtain-' + i).removeClass('up down scroll').addClass('drag').css("margin-top", px + "px");
        return false;
    };    

    setCurtainScroll = function(i, px) {
        $('.curtain-' + i).removeClass('up down drag').addClass('scroll').css("margin-top", px + "px");
        return false;
    };

    forw = function(i) {

        if (i == curtCount) {

            return rollDownCurtain(i);

        } else {

            grantActiveCurt(i + 1);
            rollUpCurtain(i);
            updateHash(i+1);
            return false;
        }

    };

    back = function(i) {

        if (i == 1) {

            return rollDownCurtain(i);

        } else {

            grantActiveCurt(i-1);
            rollDownCurtain(i-1);
            updateHash(i-1);
            return false;

        }
    };


    /* ---- Navigation Vector Methods ---- */

    /* -- Menu-based Navigation -- */
    $('.menu-curtain a:not(.parent-link):not([href=#])').on('click', function() {
        var hash = $(this).attr('href');
        hash = hash.substring(hash.indexOf("#") + 1);
        var url = window.location;
        if (url.pathname == '/') {
            location.hash = hash;
            return false;
        } else {
            $('body').fadeToggle('slow', function() {
                window.location = window.location.origin + '/#' + hash;
            });
        }
    });



    /* -- Navigation and Contextual Location -- */
    var locationHashChanged = function() {

        var hashtag = location.hash;

        $('.menu-item.act').removeClass('act');

        var linkText = $('.menu-item a[href$="' + hashtag + '"]').text();

        if (hashtag) {

            $('.tab-bar h2.entry-title').text(linkText);
            $('.menu-item a[href$="' + hashtag + '"]').parent('.menu-item').addClass('act');

            var menuIndex = $('.menu-curtain a[href$="' + hashtag + '"]').index('.menu-curtain a:not(.parent-link):not([href=#])');

            return showCurt(menuIndex + 1);

        }

    };


    $(document).ready(function(){

        locationHashChanged();

    });


    window.onhashchange = locationHashChanged;


    /* -- Mousewheel-based Navigation -- */
    curtMousewheel = function(delta) {

        var thumbOpen = $('.thumb-bank').hasClass('open');

        if (!thumbOpen) {


            var currentCurt = getCurrentActiveCurt();
            var pos = getActivePositionCurt(currentCurt);
            var viewportPer = viewport.height / 5;
            var wheelSpeed = delta ^ 350;

            /* - Scrolling Down - */
            if (delta < 0 && currentCurt !== curtCount) {

                deltaCurt = -pos - wheelSpeed;
                nextInLine = currentCurt + 1;
                var closeTop = viewport.height - viewportPer * 2;

                if (-pos > closeTop) {
                    self.forw(currentCurt);
                } else {
                    setCurtainScroll(currentCurt, -deltaCurt);
                }


            }

            /* - Scrolling Up - */
            if (delta > 0) {

                deltaCurt = pos + wheelSpeed;
                if (deltaCurt > 0) {
                    deltaCurt = 0;
                }
                nextInLine = currentCurt - 1;
                var closeBot = viewportPer * 1.5;

                if (currentCurt === 1) {

                    if (pos > -closeBot) {
                        self.back(currentCurt);    
                    } else {
                        setCurtainScroll(currentCurt, deltaCurt);
                    }

                } else if (currentCurt !== 1 && currentCurt !== curtCount) {

                    if (pos > -closeBot) {
                        self.back(currentCurt);
                    } else {
                        setCurtainScroll(currentCurt, deltaCurt);
                    }

                } else if (currentCurt === curtCount) {
                    self.back(currentCurt);
                }

            }


            /* - Re-align non-current curtains - */
           /* $('.curtain').each(function() {

                var thisNumber = getCurtainNumber(this);

                if (thisNumber < currentCurt) {
                    $('.curtain-' + thisNumber).stop().animate({
                        'margin-top': -viewport.height - 45
                    }, 500, 'easeOutQuad');
                } else if (thisNumber > currentCurt) {
                    $('.curtain-' + thisNumber).stop().animate({
                        'margin-top': 0
                    }, 500, 'easeOutQuad');
                }

            });
*/


        }

    };

    /* -- Touch-based Navigation -- */
    function handleHammerY(ev) {

        ev.gesture.preventDefault();

        var current = $(this).attr('data-curtain');
        current = Number(current);

        switch (ev.type) {
            case 'dragdown':

                p = current - 1;
                //$('.curtain-' + p).show();
                curt_drag_offset = (100 / curtHeight) * ev.gesture.deltaY;
                if ((current == 1 && ev.gesture.direction == Hammer.DIRECTION_DOWN)) {
                    curt_drag_offset *= 0;
                }
                setCurtainOffset(current - 1, -100 + curt_drag_offset, true, false);
                break;

            case 'dragup':

                curt_drag_offset = (100 / curtHeight) * ev.gesture.deltaY;
                if ((current == curtCount && ev.gesture.direction == Hammer.DIRECTION_UP)) {
                    curt_drag_offset *= 0;
                }
                setCurtainOffset(current, curt_drag_offset, true, false);
                break;

            case 'swipeup':

                ev.gesture.stopDetect();

                self.forw(current);

                break;

            case 'swipedown':

                ev.gesture.stopDetect();

                self.back(current);

                break;

            case 'release':

                if (Math.abs(ev.gesture.deltaY) > curtHeight / 4) {

                    if (ev.gesture.direction === Hammer.DIRECTION_DOWN) {
                        back(current);
                    } else {
                        forw(current);
                    }

                } else {    

                    if (ev.gesture.direction === Hammer.DIRECTION_DOWN) {

                        back(current);

                    } else {

                        rollDownCurtain(current);

                    }

                }
                ev.gesture.stopDetect();

                break;
        }
    }


    /* ---- Event Hooks ---- */
    element.hammer({
            drag_lock_to_axis: true
        })
        .on("release dragup dragdown swipeup swipedown", handleHammerY);

    $(document).bind('mousewheel', function(e) {
        curtMousewheel(e.deltaY);
        return false;
    });



};



$('section.left-small, span.left-off-canvas-menu-edit, .left-off-canvas-menu li a, .tab-bar-section .logo, a.exit-off-canvas').on('click', function() {
    $('.off-canvas-wrap').toggleClass('move-right');
});

var opened = false;

$('.pane img, .open-thumb-bank, .navi').on('click', function() {

    var opened = $('.off-canvas-wrap').hasClass('move-right');

    if (opened === true) {
        $('.off-canvas-wrap').toggleClass('move-right');
    }
    return false;
});

$('.tab-bar-section .logo').on('click', function() {
    return false;
});

/* --##-- Curtain Init --##-- */
Curtain(".container .columns[role=main] .curtain");