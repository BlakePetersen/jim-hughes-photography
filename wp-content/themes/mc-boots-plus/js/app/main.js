define([
    'jquery',
    'jqueryui',
    'modernizr',
    'fastclick',
    'foundation.core',
    'foundation.reveal',
    'hammer',
    'mobile',
    'carousel',
    'curtain'
], function($, Modernizr, FastClick) {

    var firstViewedEver = $.cookie('firstViewedEver');

    if (firstViewedEver === undefined) {

        require(['foundation.joyride'], function(){
  
            $(document).foundation({joyride: {
                tipLocation          : 'top',  // 'top' or 'bottom' in relation to parent
                nubPosition          : 'bottom',    // override on a per tooltip bases
                scrollSpeed          : 300,       // Page scrolling speed in milliseconds
                timer                : 0,         // 0 = no timer , all other numbers = timer in milliseconds
                startTimerOnClick    : true,      // true or false - true requires clicking the first button start the timer
                startOffset          : 0,         // the index of the tooltip you want to start on (index of the li)
                nextButton           : true,      // true or false to control whether a next button is used
                tipAnimation         : 'fade',    // 'pop' or 'fade' in each tip
                pauseAfter           : [],        // array of indexes where to pause the tour after
                tipAnimationFadeSpeed: 300,       // when tipAnimation = 'fade' this is speed in milliseconds for the transition
                cookieMonster        : true,     // true or false to control whether cookies are used
                cookieName           : 'joyride', // Name the cookie you'll use
                cookieDomain         : true,     // Will this cookie be attached to a domain, ie. '.notableapp.com'
                cookieExpires        : 1,       // set when you would like the cookie to expire.
                tipContainer         : 'body',    // Where will the tip be attached
                template : { // HTML segments for tip layout
                    link    : '<a href="#close" class="joyride-close-tip">&times;</a>',
                    timer   : '<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>',
                    tip     : '<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>',
                    wrapper : '<div class="joyride-content-wrapper"></div>',
                    button  : '<a href="#" class="small button joyride-next-tip"></a>'
                }
            }
            }).foundation('joyride', 'start'); 
        
        });


        $.cookie('firstViewedEver', 1, { expires: 900, path: '/' }) ;

        setTimeout(function(){

            $('.loading.logo').addClass('hide');

        }, 500);


    } else {

        $(document).foundation();

        $('.joyride-list').remove();

    }

});