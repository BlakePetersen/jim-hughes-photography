//The build will inline common dependencies into this file.

//For any third party dependencies, like jQuery, place them in the lib folder.

//Configure loading modules from the lib directory,
//except for 'app' ones, which are in a sibling
//directory.

requirejs.config({

    baseUrl: '/wp-content/themes/mc-boots-plus/js/lib',

    paths: {

        /* --  -- */
        app: '../app',
        carousel: '../app/carousel',
        curtain: '../app/curtain',

        /* -- jQuery -- */
        jquery: '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min',
        jqueryui: '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min',

        /* -- Foundation -- */
        'foundation.core': 'foundation/foundation',
        'foundation.joyride': 'foundation/foundation.joyride',
        'foundation.offcanvas': 'foundation/foundation.offcanvas',
        'foundation.reveal': 'foundation/foundation.reveal',

        /* -- Dependencies -- */
        fastclick: 'foundation/fastclick',
        modernizr: 'modernizr/modernizr',
        hammer: 'hammer/hammer',
        cookie: 'jquery/jquery.cookie',
        mousewheel: 'jquery/jquery.mousewheel',
        mobile: 'jquery/jquery.mobile.custom.min'

    },
    shim: {

        /* -- jQuery -- */
        jqueryui: { deps: ['jquery'] },

        /* -- Foundation -- */
        'foundation.core': { deps: ['jquery', 'modernizr'], exports: 'Foundation' },
        'foundation.joyride': { deps: ['foundation.core', 'cookie'] },
        'foundation.offcanvas': { deps: ['foundation.core'] },
        'foundation.reveal': { deps: ['foundation.core'] },

        /* -- Dependencies -- */
        fastclick: { exports: 'FastClick' },
        modernizr: { exports: 'Modernizr' },
        cookie: { deps: ['jquery'] },
        mousewheel: { deps: ['jquery'], exports: 'mousewheel' },
        hammer: { deps: ['jquery'], exports: 'hammer' },
        carousel: { deps: ['jquery', 'cookie', 'hammer'] },
        curtain: { deps: ['jquery','mousewheel', 'modernizr', 'hammer'] }

    }
});

requirejs(['app/main']);