/* -- Navigation and Contextual Location -- */
var locationHashChanged = function() {
    var hashtag = location.hash;
    $('.menu-item.act').removeClass('act');
    var linkText = $('.menu-item a[href$="' + hashtag + '"]').text();
    if (hashtag) {
        $('.tab-bar h2.entry-title').text(linkText);
        $('.menu-item a[href$="' + hashtag + '"]').parent('.menu-item').addClass('act');
        var menuIndex = $('.menu-curtain a[href$="' + hashtag + '"]').index('.menu-curtain a:not(.parent-link):not([href=#])');
        showCurt(menuIndex + 1);
    }
};