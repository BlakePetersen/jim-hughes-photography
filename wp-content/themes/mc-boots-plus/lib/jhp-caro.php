<?php  

	function jhpCaroScripts() {

		wp_enqueue_style( 'jhp-caro-css', get_template_directory_uri() . '/css/jhp-caro.css');
		wp_enqueue_script( 'hammer', get_template_directory_uri() . '/js/vendor/hammer.js', array(), '1.0.6', true );
		wp_enqueue_script( 'jhp-caro-js', get_template_directory_uri() . '/js/jhp/jhp-caro.js', array(), '1.0.0', true );

	}

	add_action( 'wp_enqueue_scripts', 'jhpCaroScripts' );


	function jhpCaroFunction($atts) {

		//require_once('Mobile_Detect.php');
		//$detect = new Mobile_Detect();

		extract( shortcode_atts( array( 'ids' => 'ids' ), $atts ) );

 ?>


		<div class="jhp-caro" id="carousel">

			<p class="navi back">&#xE6FD;</p>

			<ul class="main">

				<?php

					$ids = explode(',', $atts['ids']);
				 	$i = 1;


	 				//if ($detect->isMobile()) {
					//	$imgSize = 'small';
					//} else {
					//	$imgSize = ;
					//}


						foreach ($ids as $attachment_id) {

				?>
						<li class="<?php if ($i == 1 || $i == 2 || $i == 3) { echo 'on'; } ?> pane pane-<?php echo $i; ?>" data-carousel="<?php echo $i; ?>" style="z-index: <?php echo (count($ids) - $i);?>; background-image:url('<?php echo wp_get_attachment_image_src( $attachment_id, 'large' )[0]; ?>');" <?php if ($i == 1) { echo 'role="active"'; } ?> >


				 		</li>

				<?php 
						$i++;
					}  

				?>

			</ul>

			<p class="navi forw">&#xE6FC;</p>

			<p class="open-thumb-bank">&#xE627;</p>

			<div class="thumb-bank">
				
				<p class="close-thumb-bank">&#xE655;</p>

				<ul>

					<script>
						var imgCount = 0;
						imgCount = <?php $imgCount = count($ids); echo $imgCount; ?>;
					</script>

					<?php

					 	$i = 0;

						foreach ($ids as $attachment_id) {

					?>
							<li class="jhp-caro-<?php echo $i; ?>" style="background-image:url('<?php echo wp_get_attachment_image_src( $attachment_id, 'large' )[0]; ?>');">

					 		</li>

					<?php 
							
							$i++;

						}  

					?>

				</ul>

			</div>

		</div> 

		<?php

	}

	add_shortcode( 'jhp-caro', 'jhpCaroFunction' );

?>

